package finalproject;

import javax.swing.JFrame;
public class Start {
    private int type;
    private int max;
    Start(String name,int type,int max)
    {
        String name1=name;
        GameBoard.getname(name1);
        this.type=type;
        this.max=max;
        Game game = new Game(name1,type,max);
        JFrame window=new JFrame("2048");
        
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setResizable(false);
        window.add(game);
        window.pack();
        window.setLocationRelativeTo(null);
        window.setVisible(true); 
        
        game.start();
        
    }
}
