package finalproject;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Random;

import java.util.Random;
import javax.swing.JOptionPane;

public class GameBoard5 {
    public Connection cn;
    public PreparedStatement ps;
    public ResultSet rs;
    public static int ROWS=5;
    public static int COLS=5;
    
    private final int startingTiles=2;
    private Tile[][] board;
    private boolean dead;
    private boolean won;
    private BufferedImage gameBoard;
    private BufferedImage finalBoard;
    private int x;
    private int y;
    private boolean hasStarted;
     //saving
    private String saveDataPath;
    private String fileName="SaveData";
    
    private long elapsedMS;
    private long fastestMS;
    private long startTime;
    private String newtime="0000000";
    private String formattedTime="00:00:000";
    private int size,maxi;
    
     private String uname;
    private int score=0;
    private int highscore=0;
    private Font scoreFont;
    static String name1;
    
    private static int SPACING=10;
    
    
    public GameBoard5(int x, int y,int type,String name,int max){
         try{
            saveDataPath=GameBoard.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
        }
        catch(Exception e){
            e.printStackTrace();
        }
         try {
            Class.forName("com.mysql.jdbc.Driver");
            cn=DriverManager.getConnection("jdbc:mysql://localhost:3306/mp1?zeroDateTimeBehavior=convertToNull","root","");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null,ex);
        }
        
        scoreFont=Game.main.deriveFont(24f);
       uname=name;
       maxi=max;
               this.x=x;
        this.y=y;
        size=type;
        board=new Tile[ROWS][COLS];
        gameBoard=new BufferedImage(BOARD_WIDTH, BOARD_HEIGHT, BufferedImage.TYPE_INT_RGB);
        finalBoard=new BufferedImage(BOARD_WIDTH, BOARD_HEIGHT, BufferedImage.TYPE_INT_RGB);
        
        startTime=System.nanoTime(); 
//        loadHighScore();
        createBoardImage();
         start();
         if(size==0){
            ROWS=5;COLS=5;
        }
        else{
            ROWS=5;COLS=5;
        }
       
    }public static int BOARD_WIDTH=(COLS + 1)*SPACING + COLS * Tile.WIDTH;
    public static int BOARD_HEIGHT=(ROWS + 1)*SPACING + ROWS * Tile.HEIGHT;

    
    private void createSaveData(){
        try{
            File file=new File(saveDataPath, fileName);
            
            FileWriter output=new FileWriter(file);
            BufferedWriter writer=new BufferedWriter(output);
            writer.write("" + 0);
            //create fastest time
            writer.newLine();
            writer.write("" + Integer.MAX_VALUE);
            writer.close();
        }
        catch(Exception e){e.printStackTrace();}
    }
    
    private void loadHighScore(){
        try{
            File f=new File(saveDataPath, fileName);
            if(!f.isFile()){
                createSaveData();
            }
            
            BufferedReader reader=new BufferedReader(new InputStreamReader(new FileInputStream(f)));
//            highscore=Integer.parseInt(reader.readLine());
            //read fastest time
            fastestMS=Long.parseLong(reader.readLine());
           // System.out.println(fastestMS);
            reader.close();
        }
        catch(Exception e){e.printStackTrace();}
    }
    
    private void setHighScore(){
        FileWriter output=null;
        try{
            File f=new File(saveDataPath, fileName);
            output=new FileWriter(f);
            BufferedWriter writer=new BufferedWriter(output);
            
            writer.write("" + highscore);
            writer.newLine();
            if(elapsedMS<=fastestMS && won){
                writer.write("" + elapsedMS);
            }
            else{
                writer.write("" + fastestMS);
            }
            
            writer.close();
        }
        catch(Exception e){e.printStackTrace();}
    }
    private void createBoardImage(){
        Graphics2D g=(Graphics2D) gameBoard.getGraphics();
        g.setColor(Color.darkGray);
        g.fillRect(0, 0, BOARD_WIDTH, BOARD_HEIGHT);
        g.setColor(Color.lightGray);
        
        for(int row=0; row< ROWS; row++){
            for(int col=0;col<COLS;col++){
                int x=SPACING + SPACING*col + Tile.WIDTH * col;
                int y=SPACING + SPACING*row + Tile.WIDTH * row;
                g.fillRect(x, y, Tile.WIDTH, Tile.HEIGHT);
            }
        }
    }
    
    
    
    private void start(){
        for(int i=0;i<startingTiles;i++){
            spawnRandom();
        }
}
  
    
    
    private void spawnRandom(){
        Random rd=new Random();
        boolean notValid=true;
        
        while(notValid){
            int location=rd.nextInt(ROWS * COLS);
            int row=location/ROWS;
            int col=location%COLS;
            Tile current=board[row][col];
            if(current==null){
                int value = rd.nextInt(10) < 6 ? 4 : 2;
                Tile tile=new Tile(value, getTileX(col), getTileY(row));
                board[row][col]=tile;
                notValid=false;
            }
        }
    }
    
    public int getTileX(int col){
        return SPACING + col*Tile.WIDTH + col*SPACING;
    }
    
    public int getTileY(int row){
        return SPACING + row*Tile.WIDTH + row*SPACING;
    }
    
    public void render(Graphics2D g) {
        Graphics2D g2d=(Graphics2D)finalBoard.getGraphics();
        g2d.drawImage(gameBoard,0,0,null);
        
        for(int row=0;row<ROWS;row++){
            for(int col=0;col<COLS;col++){
                Tile current=board[row][col];
                if(current==null) continue;
                current.render(g2d);
            }
        }
        
        g.drawImage(finalBoard, x, y, null);
         
        g.setColor(Color.lightGray);
        g.setFont(scoreFont);
        g.drawString("" + score, 30, 40);
        g.setColor(Color.red);
        g.drawString("Best: " + maxi, Game.WIDTH - DrawUtils.getMessageWidth("Best: " + maxi, scoreFont, g) - 20, 40);
        if(dead){
            Font font=Game.main.deriveFont(55f);
            g.setFont(font);
            g.setColor(Color.yellow);
            g.drawString("Game Over", 67, 300);
             String sql="UPDATE `gamer data` SET `Score`=? WHERE `Name`=?";
        
                try{
                    ps=cn.prepareStatement(sql);
                    ps.setString(1, score+"");
                    ps.setString(2, uname);
                   ps.executeUpdate();
                }
                catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }
                String sql1="UPDATE `gamer data` SET `Time` =? WHERE `Name`=?";
                String time ="0",tmp=newtime;
              //  System.out.println(tmp);
                int j=0;
                for(int i=0;i<formattedTime.length();i++){
                    if(formattedTime.charAt(i)==':'){
                    continue;
                    }
                    else {
                        time+=formattedTime.charAt(i);
                        //time.charAt(j++)=formattedTime.charAt(i);
                    }
                }
                try{
                    ps=cn.prepareStatement(sql1);
                    ps.setString(1, time);
                    ps.setString(2, uname);
                   ps.executeUpdate();
                }
                catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }
        }
        
        Font font=Game.main.deriveFont(24f);
        g.setFont(font);
        g.setColor(Color.black);
        g.drawString("Time: "+ formattedTime, 30, 90);
        g.setColor(Color.red);
       // g.drawString("Fastest: " + formatTime(fastestMS), Game.WIDTH - DrawUtils.getMessageWidth("Fastest: " + formatTime(fastestMS), scoreFont, g)-20,90);
        setHighScore();
        g.dispose();
    }
     public static void  getname(String name){
        name1=name;
    }
    public void update(){  
        checkKeys();
        if(!won && !dead){  
            if(hasStarted){
                elapsedMS=(System.nanoTime() - startTime)/1000000;
                formattedTime=formatTime(elapsedMS);
            }
            else{
                startTime=System.nanoTime();
            }
        }
        
       // if(dead || won) setHighScore();
        if(score > highscore){ 
            highscore=score;
        }
        
//        String sql="UPDATE `gamer data` SET `Score`=? WHERE `Name`='"+name1+"' ";
        
//        try{
//            ps=cn.prepareStatement(sql);
//            ps.setString(1, highscore+"");
//           // ps.executeUpdate();
//        }
//        catch (Exception e) {
//            JOptionPane.showMessageDialog(null, e);
//        }
        for(int row=0;row<ROWS;row++){
            for(int col=0;col<COLS;col++){
                Tile current=board[row][col];
                if(current==null) continue;
                current.update();
                resetPosition(current,row,col);
                if(current.getValue()==2048)
                    won=true;
            }
        }
        //setHighScore();
    }
    
      private String formatTime(long milli){
        String formattedTime;
        
        String hourFormat="";
        int hours=(int)(milli/3600000);
        if(hours>=1){
            milli-=hours*3600000;
            if(hours<10){
                hourFormat="0" + hours;
            }
            else{
                hourFormat = "" + hours;
            }
            hourFormat +=":";
        }
        
        String minFormat="";
        int min=(int)(milli/60000);
        if(min>=1){
            milli-=min*60000;
            if(min<10){
                minFormat="0" + min;
            }
            else{
                minFormat = "" + min;
            }
        }
        else{
            minFormat="00";      
        }
        
        String secFormat="";
        int sec=(int)(milli/1000);
        if(sec>=1){
            milli-=sec*1000;
            if(sec<10){
                secFormat="0" + sec;
            }
            else{
                secFormat = "" + sec;
            }
        }
        else{
            secFormat="00";
        }
        String milliFormat;
        if(milli>99){
            milliFormat="" + milli;
        }
        else if(milli>9){
            milliFormat="0" + milli;
        }
        else{
            milliFormat="00" + milli;
        }
        newtime=hourFormat + minFormat + "" + secFormat + "" + milliFormat;
        formattedTime = minFormat + ":" + secFormat + ":" + milliFormat;
        return formattedTime;
    }
    
    private void resetPosition(Tile current, int row, int col){
        if(current==null) return;
        
        int x=getTileX(col);
        int y=getTileY(row);
        
        int distX=current.getX()-x;
        int distY=current.getY()-y;
        
        if(Math.abs(distX) < Tile.SLIDE_SPEED){
            current.setX(current.getX()-distX);
        }
        
        if(Math.abs(distY) < Tile.SLIDE_SPEED){
            current.setY(current.getY()-distY);
        }
        
        if(distX<0){
            current.setX(current.getX() + Tile.SLIDE_SPEED);
        }
        
        if(distY<0){
            current.setY(current.getY() + Tile.SLIDE_SPEED);
        }
        
        if(distX>0){
            current.setX(current.getX() - Tile.SLIDE_SPEED);
        }
        
        if(distY>0){
            current.setY(current.getY() - Tile.SLIDE_SPEED);
        }
        
    }
    
    private boolean move(int row, int col, int hDir, int vDir, Direction dir){
        boolean canMove=false;
        Tile current=board[row][col];
        if(current==null) return false;
        boolean move=true;
        int newCol=col;
        int newRow=row;
        while(move){
            newCol+=hDir;
            newRow+=vDir;
            if(checkOutOfBounds(dir, newRow, newCol)) break;
            if(board[newRow][newCol]==null){
                board[newRow][newCol]=current;
                board[newRow-vDir][newCol-hDir]=null;
                board[newRow][newCol].setSlideTo(new Point(newRow, newCol));
                canMove=true;
            }
            else if(board[newRow][newCol].getValue() == current.getValue() && board[newRow][newCol].canCombine()){
                board[newRow][newCol].setCanCombine(false);
                board[newRow][newCol].setValue(board[newRow][newCol].getValue() * 2);
                canMove=true;
                board[newRow-vDir][newCol-hDir]=null;
                board[newRow][newCol].setSlideTo(new Point(newRow, newCol));
                board[newRow][newCol].setCombineAnimation(true);
                //add to score
                score += board[newRow][newCol].getValue();
                
            }
            else{
                move=false;
            }
        }
        return canMove;
    }
    
    private boolean checkOutOfBounds(Direction dir, int row, int col){
        if(dir==Direction.LEFT){
            return col<0;
        }
        
        else if(dir==Direction.RIGHT){
            return col>COLS-1;
        }
        
        else if(dir==Direction.UP){
            return row<0;
        }
        
        else if(dir==Direction.DOWN){
            return row>ROWS-1;
        }
        return false;
    }
    
    private void moveTiles(Direction dir){
        boolean canMove=false;
        int hDir=0;
        int vDir=0;
        
        if(dir==Direction.LEFT){
            hDir=-1;
            for(int row=0;row<ROWS;row++){
                for(int col=0;col<COLS;col++){
                    if(!canMove){
                        canMove=move(row, col, hDir, vDir, dir);
                    }
                    else move(row, col, hDir, vDir, dir);
                }
            }
        }
        
        else if(dir==Direction.RIGHT){
            hDir=1;
            for(int row=0;row<ROWS;row++){
                for(int col=COLS-1;col>=0;col--){
                    if(!canMove){
                        canMove=move(row, col, hDir, vDir, dir);
                    }
                    else move(row, col, hDir, vDir, dir);
                }
            }
        }
        
        else if(dir==Direction.UP){
            vDir=-1;
            for(int row=0;row<ROWS;row++){
                for(int col=0;col<COLS;col++){
                    if(!canMove){
                        canMove=move(row, col, hDir, vDir, dir);
                    }
                    else move(row, col, hDir, vDir, dir);
                }
            }
        }
        
        else if(dir==Direction.DOWN){
            vDir=1;
            for(int row=ROWS-1;row>=0;row--){
                for(int col=0;col<COLS;col++){
                    if(!canMove){
                        canMove=move(row, col, hDir, vDir, dir);
                    }
                    else move(row, col, hDir, vDir, dir);
                }
            }
        }
        
        else{
            System.out.println(dir + " is not valid");
        }
        
        for(int row=ROWS-1;row>=0;row--){
            for(int col=0;col<COLS;col++){
                Tile current=board[row][col];
                if(current==null) continue;
                current.setCanCombine(true);
            }
        }
        
        if(canMove){
            spawnRandom();
            checkDead();
        }
    }
        private void checkDead(){
            for(int row=0;row<ROWS;row++){
                for(int col=0;col<COLS;col++){
                    if(board[row][col]==null) return;
                    if(checkSurroundingTiles(row,col,board[row][col])){
                        return;
                    }
                }
            }
            dead=true;
           setHighScore();
        }
        

    private boolean checkSurroundingTiles(int row, int col, Tile current){
        if(row>0){
            Tile check=board[row-1][col];
            if(check==null) return true;
            if(current.getValue()==check.getValue()) return true;
            
        }
        if(row < ROWS-1){
            Tile check=board[row+1][col];
            if(check==null)return true;
            if(current.getValue()==check.getValue()) return true;
        }
        if(col > 0){
            Tile check=board[row][col-1];
            if(check==null)return true;
            if(current.getValue()==check.getValue()) return true;
        }
        if(col < COLS-1){
            Tile check=board[row][col+1];
            if(check==null)return true;
            if(current.getValue()==check.getValue()) return true;
        }
        return false;
    }

    private void checkKeys(){
        if(Keyboard.typed(KeyEvent.VK_LEFT)){
            moveTiles(Direction.LEFT);
            //System.out.println("LEFT pressed");
            if(!hasStarted) hasStarted=true;
        }
        
        if(Keyboard.typed(KeyEvent.VK_RIGHT)){
            moveTiles(Direction.RIGHT);
            if(!hasStarted) hasStarted=true;
        }
        
        if(Keyboard.typed(KeyEvent.VK_UP)){
            moveTiles(Direction.UP);
            if(!hasStarted) hasStarted=true;
        }
        
        if(Keyboard.typed(KeyEvent.VK_DOWN)){
            moveTiles(Direction.DOWN);
            if(!hasStarted) hasStarted=true;
            
        }
        
    }
}
