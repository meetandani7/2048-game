package finalproject;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

public class Tile {
    public static final int WIDTH=70;
    public static final int HEIGHT=70;
    public static final int SLIDE_SPEED=30;
    
    private int value;
    private BufferedImage tileImage;
    public static BufferedImage tImage;

    private Color bg;
    private Color text;
    private Font font;
    private int x,y;
    private Point slideTo;

    private boolean startAnimation=true;
    private double scaleFirst=0.1;
    private BufferedImage startImage;
    
    private boolean combineAnimation=false;
    private double scaleCombine=1.2;
    private BufferedImage combineImage;
    
    private boolean canCombine=true;

    public boolean combineAnimation() {
        return combineAnimation;
    }

    public void setCombineAnimation(boolean combineAnimation) {
        this.combineAnimation = combineAnimation;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
        drawImage();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Point getSlideTo() {
        return slideTo;
    }

    public void setSlideTo(Point slideTo) {
        this.slideTo = slideTo;
    }

    public boolean canCombine() {
        return canCombine;
    }

    public void setCanCombine(boolean canCombine) {
        this.canCombine = canCombine;
    }
    
    public Tile(int value, int x, int y){
        this.value=value;
        this.x=x;
        this.y=y;
        slideTo=new Point(x,y);
        tileImage = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);
        startImage=new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);
        combineImage=new BufferedImage(WIDTH*2, HEIGHT*2, BufferedImage.TYPE_INT_ARGB);
        drawImage();
    }
    
    private void drawImage(){
        Graphics2D g=(Graphics2D) tileImage.getGraphics();
        if(value==2){
            bg=new Color(0xe9e9e9);
            text =new Color(0x000000);
        }
        else if(value==4){
            bg=new Color(0xe6daab);
            text=new Color(0x000000);
        }
        else if(value==8){
            bg=new Color(0xf79d3d);
            text=new Color(0xffffff);
        }
        else if(value==16){
            bg=new Color(0xf28007);
            text=new Color(0xffffff);
        }
        else if(value==32){
            bg=new Color(0xf55e3b);
            text=new Color(0xffffff);
        }
        else if(value==64){
            bg=new Color(0xff0000);
            text=new Color(0xffffff);
        }
        else if(value==128){
            bg=new Color(0xe9de84);
            text=new Color(0xffffff);
        }
        else if(value==256){
            bg=new Color(0xf6e873);
            text=new Color(0xffffff);
        }
        else if(value==512){
            bg=new Color(0xf5e455);
            text=new Color(0x000000);
        }
        else if(value==1024){
            bg=new Color(0xf7e12c);
            text=new Color(0xffffff);
        }
        else{
            bg=new Color(0xffe400);
            text=new Color(0xffffff);
        }
        
        g.setColor(new Color(0,0,0,0));
        g.fillRect(0, 0, WIDTH, HEIGHT);
        g.setColor(bg);
        g.fillRect(0, 0, WIDTH, HEIGHT);
        
        g.setColor(text);
  
            font=Game.main;
       
        g.setFont(font);
        
        int drawX=WIDTH/2 - DrawUtils.getMessageWidth("" + value, font, g)/2;
        int drawY=HEIGHT/2 ;//- DrawUtils.getMessageHeight("" + value, font, g)/2;
        g.drawString("" + value, drawX, drawY);
        update();
    }
    
    public void update(){
        if(startAnimation){
            AffineTransform transform=new AffineTransform();
            transform.translate(WIDTH/2 - scaleFirst*WIDTH/2, HEIGHT/2 - scaleFirst*HEIGHT/2);
            transform.scale(scaleFirst, scaleFirst);
            Graphics2D g2d=(Graphics2D)startImage.getGraphics();
            g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            g2d.setColor(new Color(0,0,0,0));
            //System.out.println("animation");
            g2d.fillRect(0, 0, WIDTH, HEIGHT);
            g2d.drawImage(tileImage, transform, null);
            scaleFirst+=0.1;
            g2d.dispose();
            if(scaleFirst>=1) startAnimation=false;
        }
        
        if(combineAnimation)
        {
            AffineTransform transform=new AffineTransform();
            transform.translate(WIDTH/2 - scaleCombine*WIDTH/2, HEIGHT/2 - scaleCombine*HEIGHT/2);
            transform.scale(scaleCombine, scaleCombine);
            Graphics2D g2d=(Graphics2D)combineImage.getGraphics();
            g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            g2d.setColor(new Color(0,0,0,0));
            g2d.fillRect(0, 0, WIDTH, HEIGHT);
            g2d.drawImage(tileImage, transform, null);
            //System.out.println("animation");
            scaleCombine-=0.05;
            g2d.dispose();
            if(scaleCombine<=1) combineAnimation=false;
        }
    }
    
    public void render(Graphics2D g){
        if(startAnimation){
            g.drawImage(startImage, x, y, null);
        }
        
        else if(combineAnimation){
            g.drawImage(combineImage, (int)(x + WIDTH/2 - scaleCombine*WIDTH/2), (int)(y + HEIGHT/2 - scaleCombine*HEIGHT/2), null);
        }
        
        else{
            g.drawImage(tileImage, x, y, null);
        }
        
        g.drawImage(tileImage, x, y, null);
        
    }
}

