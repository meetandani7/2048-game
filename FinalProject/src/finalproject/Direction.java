package finalproject;

public enum Direction {
    LEFT,
    RIGHT,
    UP,
    DOWN
}
